



# import HTTP
# import HTTP:startwrite
# import sockets

import websockets as ws
import asyncio,inspect
import json

from random import randint
from time import time,sleep
from copy import copy

import logging

thisDomain = "py"


##############################
######### HTML host  #########
##############################
#<<<<<(change to flask)

# map file extentions to MIME types
mimeTypes = {
    ".ico" : "image/x-icon",
    ".html": "text/html",
    ".js"  : "text/javascript",
    ".json": "application/json",
    ".css" : "text/css",
    ".png" : "image/png",
    ".jpg" : "image/jpeg",
    ".wav" : "audio/wav",
    ".mp3" : "audio/mpeg",
    ".svg" : "image/svg+xml",
    ".pdf" : "application/pdf",
    ".doc" : "application/msword",
    ".eot" : "appliaction/vnd.ms-fontobject",
    ".ttf" : "aplication/font-sfnt"
}


domains      = {}
servers      = set([])
clients      = set([])
#legalModules = Dict{String,Any}() #<<< used?


strDocs = {}


ROUTERDELAY = 0.3 #seconds   how frequently router checks incoming values
router = {}


# struct retValue
    # value::Any



def getExtDomains():
    return domains


#HTTP.listen("127.0.0.1", 8081) do http
#    
#    @show HTTP.URIs.splitpath(http.target)
#    
#    HTTP.setheader(http, "Content-Type" => "text/html")
#    write(http, "target uri: $(http.message.target)<BR>")
#    write(http, "request body:<BR><PRE>")
#    write(http, read(http))
#    write(http, "</PRE>")
#    return
#

#HTTP.listen("127.0.0.1", 8081) do http
#    @show http.message
#    @show HTTP.header(http, "Content-Type")
#    while !eof(http)
#        println("body data: ", String(readavailable(http)))
#    
#    HTTP.setstatus(http, 404)
#    HTTP.setheader(http, "Foo-Header" => "bar")
#    startwrite(http)
#    write(http, "response body")
#    write(http, "more response body")
#
#

#crude parsing function. The splitpath function in HTTP module doesn't seem to work for this purpose.
def parseUrl(url):
    parts = split(url,"/")
    Parts = split(parts,"?")
    if length(Parts)<2 :
        params = nothing
    else:
        paramsS = Parts[2]
        if length(paramsS)>0:
            mid     = split(paramsS,"&")
            params  = {}
            paramsL = []
            for pair in mid:
                items = split(pair,"=")
                if length(items)<2: continue;  #invalid parameter
                k,v = items
                if length(k)==0 or length(v)==0: continue;  #invalid parameter

                params[k] = v
            
        else:
            params = nothing
        
    
    
    doc
    docName = Parts[1]
    
    return (docName,params)


#

def parseUrl2(url):
    url =  url[2:] if startswith(url,"/") else url 
    
    paramSplit = split(url,"?")
    if length(paramSplit)<2 :
        params = nothing
    else:
        paramsS = paramSplit[2]
        if length(paramsS)>0:
            mid     = split(paramsS,"&")
            params  = {}
            paramsL = []
            for pair in mid:
                items = split(pair,"=")
                if length(items)<2: continue;  #invalid parameter
                k,v = items
                if length(k)==0 or length(v)==0: continue;  #invalid parameter

                params[k] = v
            
        else:
            params = nothing
        
    
    
    
    path  = paramSplit[1]
    parts = split(path,"/")
    
    return {
        "fullPath":path
        ,"docName":parts[-1]
        ,"params":params
        ,"parts":parts
    }



def httpRequestHandler(req):
    
    #println("tar ",req.target)
    #@show pwd()
    #for a in HTTP.URIs.splitpath(req.target)
    #    println("tt",a)
    #
    
    
    parsedUrl = parseUrl2(req.target)
    #println(parsedUrl["docName"])
    #println(parsedUrl["fullPath"])
    #@show parsedUrl
    try:
        
        with open("./"*parsedUrl["fullPath"]) as f:
            s = f.read()
        
        
        #@show s
        path,ext = splitext(parsedUrl["docName"])
        #@info ext
        mime = mimeTypes[ext]
        
        res = HTTP.Response(200, s)
        HTTP.setheader(res, {"Content-Type" : mime})
        return res
    except Exception as e:
        print(e)
        return HTTP.Response(404)
    
    
    #dd  = Dict("a"=>5)
    #ddj = json(dd)
    #println("js ",ddj)
    #
    #
    ##HTTP.setstatus(http, 404)
    #
    #res = HTTP.Response(200, ddj)
    #HTTP.setheader(res, "Content-Type" => "text/plain")
    #res



def ver():
    println("ver",1)
    


def docs():
    return strDocs



#create or update string document in memory
def strDocInitHandler(req):

    doc = J.parse(IOBuffer(HTTP.payload(req)))
    
    #doc = J.parse(String(HTTP.payload(req)))
    #@info "strReq5" doc
    
    
    
    mime = "text/html"
    res = HTTP.Response(200, "ok")
    HTTP.setheader(res, {"Content-Type": mime})
    
    strDocs[doc["name"]] = doc["content"]
    
    return res


def strDocRequestHandler(req):
    try:
        #println("strtar ",req.target)
        
        
        pieces = HTTP.URIs.splitpath(req.target)
        #@info "pieces" pieces
        strDoc = strDocs[pieces[2]]
        
        mime = "text/html"
        res = HTTP.Response(200, strDoc)
        HTTP.setheader(res, {"Content-Type": mime})
        return res
        
    except Exception as e:
        print( e)
        return HTTP.Response(404)
    
    
    



#returns a handler function that serves files from path0
def routingPath(path0):
    def pathRequestHandler(req):
    
    
        parsedUrl = parseUrl2(req.target)
        
        #@info path0 parsedUrl["parts"][2:]
        filePath = joinpath( path0, *parsedUrl["parts"][2:] )
        
        
        with open(filePath) as f:
            s = f.read()
        
        path,ext = splitext(parsedUrl["docName"])
        
        mime = mimeTypes[ext]
        
        res = HTTP.Response(200, s)
        HTTP.setheader(res,  {"Content-Type": mime})
        return res
    
        
        
        return HTTP.Response(404)
        
     
    return pathRequestHandler





###################################
######### Domain handling #########
###################################

#<<< not used yet.
#keeps track of everything and handles calls between domains
#Also maintains a background thread for async processes and manages safe calls from synchronous python threads.
class Manager():
    def __init__(self):
        self.router = {}
        self.domains = {}
        self.routerDelay = 500
        self.eventLoop = None
        self.thread = None
    def handleMessage(self):
        pass




class LocalDomain():
    def __init__(self,domainName="py",modules={}):
        self.domainName = domainName
        self.modules    = modules
        # domainName::String
        # modules::Dict{String,Module}
        # LocalDomain() = new()
        # LocalDomain(domainName::String,modules::Dict{String,Module}) = new(domainName,modules)



    def registerModule_(self, modName, module_):
        pass

    #external domain has made a call to python
    async def handleCall(self, data):
        #make a call to the requested function
        
        modRef = self.modules[data["module"]]
        
        fun = getattr(modRef,data["call"])
        ret = fun( *data["args"] )
        # logging.debug(f"ways: returning {ret}")

        #init a return object
        returnDat = {
            "event"    : "return",
            "src"      : data["src"],
            "route_id" : data["route_id"],
        }
        
        if inspect.isawaitable(ret):
            returnDat["return"] = await ret
        else:
            returnDat["return"] = ret
                        
        
        await domains[data["src"]].handleReturn(returnDat)
            
        

    #NOTE: handleCall of LocalDomain above does not usually call this! handleCall calls a matching method where domain is of different type (e.g. WsServerConnection).
    #This is called when call to an external function returns (original call was made by julia)
    #Original call set up a Task that monitors router table, and resolves once
    #the right route_id is found.
    async def handleReturn(self, data):
        await router[data["route_id"]].put(data["return"])


    async def disconnect(self):
        #empty dummy function to simplify mass restart.
        pass






#object initializer cannot be asynchronous, so this wrapper runs some post init setup
async def create_WebSocketServer(*args):
    self = WebSocketServer(*args)
    await self.server #ensure the server actually starts up
    return self
class WebSocketServer():
    def __init__(self,legalDomains,port):
        #self = WebSocketServer()
        self.clients = {}
        
        #The server will reject any identifier, that is not in legalDomains list.
        self.legalDomains = copy(legalDomains) 
        
        logging.debug("ways: starting websocket server")
        
        #Dummy handler for http requests to localhost:port
        #Http requests are handled separately. Nothing should ever call this.
        # httpDummy(req) = WebSockets.Response("httpdummy")
    
        self.server = ws.serve(self.handler, "localhost", port)
        
        #This is technically a blocking call, but evidently it returns immediately and creates an async background task of some sort.
        #I don't know how it actually works.
        #asyncio.get_event_loop().run_until_complete(self.server)

        #asyncio.run_coroutine_threadsafe(gg(),loop)
        
        # asyncio.get_event_loop().create_task(asyncio.ensure_future(self.server))
        #self.ws_server.close()
   

    #deals with client connections
    async def handler(self, websocket, path):
        connection = WsServerConnection(websocket,self)
        try:
            while True:
                msg = await websocket.recv()
                await handleMessage(msg,connection)
                # print("==============")
                # print(f"server: got {msg}")
        except ws.exceptions.ConnectionClosedOK as e:
            #Called when client disconnects cleanly. Apparently that's an error.
            pass
        except ws.exceptions.ConnectionClosedError as e:
            #Called when client drops ungracefully. Not really an issue here.
            logging.debug(f"ways: error: {e}")
            pass
        finally:
            logging.debug(f"ways: domain '{connection.domainName}' disconnected")
            domains.pop(connection.domainName)
    

    def disconnect(self): #::WebSocketServer
        logging.debug("ways: server closing...")
        # for (domName,connection) in self.clients:
            # connection.disconnect()
        
        self.server.ws_server.close()
        servers.remove(self)




#Wrapper to the socket that we push data to, when interacting with the client.
class WsServerConnection():
    def __init__(self,socket,server,domainName=""):
        
        self.socket     = socket
        self.server     = server
        self.domainName = domainName #not necessarily set before handshake

    #python code is calling a function in external domain. 
    #Note, that returning value goes to the routing table via handleReturn(::LocalDomain...), and is read from there by the "callDomain" function, which called this function.
    #  callDomain starts an async task for this.
    #The handleReturn function below is not involved in this process. Perhaps these are confusing names.
    async def handleCall(self, data):
        await self.socket.send(json.dumps(data))
        #<<<This is seriously bugging me. 
        #   HandleCall should wait for response via the router and return the value directly to the caller.
        #   The indirect way it's currently done is really confusing and hard to follow.


    #external websocket domain has previously called python function (or there has been a domain hop). 
    #Now we are returning a value to that domain.
    async def handleReturn(self, data):
        await self.socket.send(json.dumps(data))

    #finalize handshake and respond to the client
    async def finalizeHandshake(self, messageData):
    
        if messageData["src"] in self.server.legalDomains: #is the connecting domain recognizable?
        
            #<<< frankly I don't recall why there is fromDomain separately from src
            handshakeObject = {"event":"handshake", "fromDomain":thisDomain, "src":thisDomain} 
            logging.debug(f"ways: responding to handshake from '{messageData['src']}' with: {handshakeObject}")
        
            domains[messageData["src"]] = self            #register this connection as domain
            self.domainName             = messageData["src"]
            
            await self.socket.send(json.dumps(handshakeObject))
        else:
            logging.info(f'invalid domain {messageData["src"]}. expected one of: {self.server.legalDomains}')

    #<<<<<
    async def disconnect(self):
        await self.socket.close()
        #domains.pop(self.domainName)
        
        
        
        
#object initializer cannot be asynchronous, so this wrapper runs some post init setup
async def create_WebSocketClient(*args):
    self        = WebSocketClient(*args)
    self.socket = await ws.connect(self.uri)
    asyncio.create_task(self.recvLoop())
    status = await self.requestHandshake()
    # print("ways: WebSocketClient connected")
    logging.debug(f"ways: WebSocketClient connected: {status}")
    return self
    
#Wrapper to the socket that we push data to, when interacting with the server.
class WebSocketClient():
    def __init__(self,domainName,port):
        self.uri  = f"ws://localhost:{port}"
        self.port = port
        self.domainName = domainName
        #NOTE: see above for async initialization
        
    async def recvLoop(self):
        #iteration blocks until next one is received. loops indefinitely.
        async for msg in self.socket: 
            # print("MSG!!!",msg)
            # logging.debug(f"MSG!!!, {msg}")
            await handleMessage(msg,self)
        
    #python code is calling a function in external domain. 
    #Note, that returning value goes to the routing table via handleReturn(::LocalDomain...), and is read from there by the "callDomain" function, which called this function.
    #  callDomain starts an async task for this.
    #The handleReturn function below is not involved in this process. Perhaps these are confusing names.
    async def handleCall(self, data):
        logging.debug(f"ways: wsclient: handleCall {data}")
        await self.socket.send(json.dumps(data))

    #external websocket domain has previously called python function (or there has been a domain hop). 
    #Now we are returning a value to that domain.
    async def handleReturn(self, data):
        await self.socket.send(json.dumps(data))

    async def requestHandshake(self):
        #let d = new Date();
        data = {
            "event": "handshake",
            "src"  : thisDomain,
            #time : d.getHours()+":"+d.getMinutes()+":"+d.getSeconds()
        }
        self.handshakeResponse = asyncio.Queue()
        
        #print("requesting handshake")
        await self.socket.send(json.dumps(data));
        
        return await self.handshakeResponse.get() #blocks until response is received
        
    #finalize handshake (called after server has confirmed handshake request)
    async def finalizeHandshake(self, messageData):
        try:
            logging.debug(f"ways: handshake from: {messageData['src']} (port:{self.port}). Registering as {self.domainName}")
            domains[self.domainName] = self   #register this connection as domain
            await self.handshakeResponse.put(True)
        except Exception as e:
            logging.debug(e)
            await self.handshakeResponse.put(False)
        #domains[messageData["src"]] = self   #register this connection as domain
        
    
    #<<<<<
    async def disconnect(self):
        await self.socket.close()
        
        
        




def getUniqueId():
    return randint(0,2**63 - 1) #upper bound is max 64 bit signed integer value


def initLocalDomain(domain):
    global thisDomain
    thisDomain = domain
    loc = LocalDomain()
    loc.domainName = domain
    domains[domain] = loc

#Register python module in the given domain.
#Multiple local domains can be registered with different names and different associated modules
#Usually ony one is enough
def registerModule(domain,moduleRegName,module_):
    #Register domain module path for given module
    #Module registration name can be different from the actual module name.
    #e.g. jl.main:mymodule
    #domainName = "$domain:$moduleRegName"

    #Add a domain and a module, or just a module to existing domain.
    if not domain in domains:
        domains[domain] = LocalDomain(domain,{moduleRegName:module_})
    else:
        domains[domain].modules[moduleRegName] = module_
    
    
    



async def registerDomains(domainNames,domainType,port):
    if (domainType=="webSocketServer"):
        srv = await create_WebSocketServer(domainNames,port)
        servers.add(srv)
        return srv
        
    #some other process maintains websocket server
    elif (domainType=="webSocketClient"):
        client = await create_WebSocketClient(domainNames[0],port)
        clients.add(client)
        return client
        # domains[domainName] = new TcpHandler("localhost",port)
        
    # #some other process maintains tcp server
    # elif (domainType=="tcpClient"):
        # return TcpHandler("localhost",port)
        # # domains[domainName] = new TcpHandler("localhost",port)
        
    # #node maintains tcp server, some other process is client
    # elif (domainType=="tcpServer"):
        # srv = TcpServer(domainNames,"localhost",port)
        # servers.add(srv)
        # return srv
        # # domains[domainName] = new TcpServer("localhost",port)
    else:
        raise Exception(f"unknown type identifier: {domainType}")


#closes all connections and clears all domains
async def unregisterAll():
    for (name,dom) in list(domains.items()):
        await dom.disconnect()
        #domains.pop(name)
    for srv in list(servers):
        srv.disconnect()
        
    return True






#Message json data is prefixed with target domain name.
#This allows quick target name parsing for domain hopping without having to parse the information from json.
#<<<NOTE: this is not fully implemented. 
#         still missing: forwarding/re-returning calls, 
#                        returning json is not prefixed with target domain.
def splitMessage(message):
    e = 1
    l = len(message)
    while e<l and message[e]!='{':
        e+=1
    
    domain = message[:e]
    dataS  = message[e:l]

    return (domain,dataS)


#Messages first arrive at registered server-capable domains (localdomain, websocketserver, tcpserver)
#Message is received in full by those specific handlers, and then passed here.
#  (How message receiving is handled depends on the protocol)
# usual pathway might be e.g.
#  -websocket client (e.g. browser) sends message to julia websocket server, where a WebsocketClientConnection receives it.
#  -handleMessage (the function below) asynchronously calls LocalDomain (wrapping python module), which calls the given function.
#  -LocalDomain waits for the function to return and then calls WebsocketClientConnection return handler directly. (handleMessage receives nothing) 
#  -WebsocketClientConnection sends message to the original websocket.
#  -On browser side, the message is interpreted as a return value to the original function
#     -calls do not block each other. Any number of async calls can be made indepently, and return in any order.
#     -a routing table is used to match return message to their calling functions. 
async def handleMessage(message,caller):
    #message is likely a handshake. The client doesn't know host domain name yet, so domain target is omitted.
    if message[0] == '{':
        targetDomain = thisDomain
        data = json.loads(message)
        dataS = ""
    else:
        targetDomain,dataS = splitMessage(message)
        #@info "dump" dataS
        data = json.loads(dataS)
    
    #print("ways: Handling:",message)
    
    #@info "tardomain" targetDomain thisDomain data
    if targetDomain==thisDomain:
        if data["event"]=="callDomain":
            #<<<add check for domain validity
            # logging.debug(f"ways: handlecall start")
            asyncio.create_task(domains[data["tar"]].handleCall(data))
            # logging.debug("ways: handlecall handled")
            
        
        elif data["event"]=="return":
            #Note: src is the domain that made the call. In most (all?) cases this is thisDomain, and domains[data["src"]] is a LocalDomain.
            #<<<this probably could be hardcoded since no other domain handles return here.
            #<<<  let's note that if there are multiple py domains, they need to use different identifiers.
            
            await domains[data["src"]].handleReturn(data)
            
            
        elif data["event"]=="callEvent":
            return 
        elif data["event"]=="handshake":
            await caller.finalizeHandshake(data)        
        
    else:
        #currently domain hopping is unhandled
        raise Exception(f"ways: unknown domain '{targetDomain}'.")
    





  
#do a quasi-synchronous call to external function
async def callDomain(domain,module_,fun,args):
    id  = getUniqueId()
    sid = str(id)
    
    ob = {
       "tar"     :domain,
       "src"     :thisDomain,
       "event"   :"callDomain",
       "args"    :args,
       "module"  :module_,
       "call"    :fun,
       "route_id":sid,
    }
    
    #Insert a queue object into the routing table.
    #LocalDomain.handleReturn will put() a value into it later
    router[sid] = asyncio.Queue()
    
    #Call external domain.
    #This call doesn't actually return anything as it ends by sending a message to the domain.
    #The domain later sends response, which needs to be interpreted as a return to this call.
    asyncio.create_task(domains[domain].handleCall(ob))
    
    #wait for the function to return (i.e. wait for the outer domain to send a response message)
    try:
        ret = await asyncio.wait_for(router[sid].get(),30)
        router.pop(sid)
    except asyncio.TimeoutError:
        logging.debug(ob["call"],"got bored after 30 seconds....")
        ret = -99999999
    
    return ret
    
    
#<<< may or may NOT work in any given situation.
#    Should work in regular scripts, but does not work in jupyter (hangs indefinitely).
#    There's some really odd timing issue, where the Future is not fully initialized before result() is called.
#Synchronous callDomain. Calls a coroutine in the manager thread, and waits for result.
def callDomainSync(*input,loop=None):
    if not loop:
        #manager = getManager().eventLoop
        loop = asyncio.get_event_loop()
    #returns a concurrent.futures.Future, NOT asyncio.Future.
    future = asyncio.run_coroutine_threadsafe( callDomain(*input) ,loop )
    return future.result() #blocks until there is a result.
    
    
    

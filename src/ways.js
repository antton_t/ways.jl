//this is an es6 module




function gotoBottom(id){
   var element = document.getElementById(id);
   element.scrollTop = element.scrollHeight - element.clientHeight;
}


function hijackErrorStream(){
    var former = console.log;
    console.log = function(msg){
        //former(msg);  //maintains existing logging via the console.
        $("#errlog").append("<div>" + msg + "</div>");

        gotoBottom("errlog");
    }
    window.onerror = function(message, url, linenumber) {
        console.log("JavaScript error: " + message + " on line " + 
                linenumber + " for " + url);
    }
}


//get GET parameters from url
export function urlGetParams(){
    var queryDict = {};
    
    location.search.substr(1).split("&").forEach(function(item) {
        if (item.length>0){
            var pair = item.split("=");
            queryDict[pair[0]] = decodeURIComponent(pair[1]);
        }
    });
    
    return queryDict;
}



//////////////////////////////////////
//some helpers

export function getRandomInt(min, max) {
    var min = Math.ceil(min);
    var max = Math.floor(max);
    return Math.floor(Math.random() * (max - min)) + min; //The maximum is exclusive and the minimum is inclusive
}
export function getUniqueId() {
    //get 16 digit random integer within safe int range
    var min = 1000000000000000;
    var max = 9000000000000000;
    return Math.floor(Math.random() * (max - min)) + min; //The maximum is exclusive and the minimum is inclusive
}
export function shuffle(a) {
    var j, x, i;
    for (i = a.length - 1; i > 0; i--) {
        j = Math.floor(Math.random() * (i + 1));
        x = a[i];
        a[i] = a[j];
        a[j] = x;
    }
}
///////


var comRoot = {
     "ready"     : false
    ,"callQueue" : []
    ,"id"        : null //id of this domain
    ,"modules"   : {}   //registered modules
    ,"hostAlias" : {}   
};
var router = {}
var comSocket = null;

export function getSelfDomainId(){
    return comRoot.id
}


export function registerModule(modName,mod){
    comRoot["modules"][modName] = mod
}

export function setHostAlias(hostAlias,realHostName){
    comRoot["hostAlias"][hostAlias] = realHostName
}


//<<< this doesn't follow the current initializing convention (Style-wise. Works correctly, though)
export function initSocket(port,domainIdSelf){
    if (comSocket!==null){
        comSocket.close()
    }
    comSocket = new WebSocket("ws://localhost:"+port);
    
    
    
    comRoot.id = domainIdSelf
    
    comSocket.addEventListener('message', function (event) {
        var dat = JSON.parse(event.data)

        //When host or middle manager calls js function.
        //The function must exists in the visible scope
        if(dat.event === "callDomain"){
            let ret
            if (dat.module==="") //calling in global scope
                ret = window[dat.call].apply(this,dat.args); //"this" is the websocket connection. Just cause some "this" is required
            else{                //calling in an es6 module (must be registered beforehand)
                ret = comRoot.modules[dat.module][dat.call].apply(this,dat.args); 
            }
            var returnDat = {
                event    : "return",
                src      : dat.src,
                route_id : dat.route_id,
            }
            
            //If the function returns a promise, wait for it to resolve before sending return object.
            //Otherwise send it immediately.
            //ES7 browsers could use async await, but that's not universally supported yet
            if (Promise.resolve(ret)===ret){
                ret.then((pret)=>{
                    if (pret===undefined)
                        pret = null
                    returnDat.return = pret
                    comSocket.send(JSON.stringify(returnDat))
                })
            }else{
                if (ret===undefined)
                    ret = null
                returnDat.return = ret
                comSocket.send(JSON.stringify(returnDat))
            }
            
            
        }
        //when host or middle manager calls js function as an event
        else if(dat.event === "callEvent"){
            var ev = new CustomEvent(dat.call,  { detail: dat.args });
            window.dispatchEvent(ev);
        }
        else if(dat.event === "return"){
            router[dat["route_id"]] = dat.return;
        }
        if(dat.event === "callAsync"){ //call, but do not return a value
            window[dat.call].apply(this,dat.args);
        }
        //getting a response to a handshake request
        else if (dat.event === "handshake"){
            //$("#msg").text("handshake from: "+dat.fromDomain);
            console.log("handshake from: "+dat.fromDomain);
            comRoot.ready = true;
            
        }
        else if(dat.event === "msg"){
            console.log("message: "+dat.val)
        }
    });

    //Called when the websocket connection is live. Send a handshake request to host process.
    comSocket.addEventListener("open",function(event){

        //$("#comstatus").text("status: "+comSocket.readyState);
        let d = new Date();
        var ob = {
            event:"handshake",
            src:domainIdSelf,
            time: d.getHours()+":"+d.getMinutes()+":"+d.getSeconds()
        }
        
        //console.log("requesting handshake")
        comSocket.send(JSON.stringify(ob));
    })
    
    comSocket.addEventListener("close", function(event){
        console.log("socket closed")
        
        //initSocket(port)
        // var retry = function()
    
    })


    
    return promiseConnection()
}

function promiseConnection(){
    return new Promise(resolve => {
        var wait = function(){
            //if (comSocket.readyState === 1){ //does not wait for handshake
            //    resolve(comSocket);
            //}
            if (comRoot.ready){ //wait for handshake
                resolve(comSocket);
            }
            else
                setTimeout(wait,50);
        }
        setTimeout(wait, 50);
    });
}



///////////////



export function callDomain(domain,module,fun,args){
 
    var id = getUniqueId();
    var sid = ""+id;
    // console.log("calling from "+comRoot.id)
    
    var ob = {
        "tar":domain,
        "module":module,
        "src":comRoot.id,
        "event":"callDomain",
        "args":args,
        "call":fun,
        "route_id":id,
    }
    router[sid] = undefined
    if (!comRoot.ready){
        console.log("host not ready, call queued",domain, fun)
        comRoot.callQueue.push(ob);
    }else{
        // console.log("calling function in domain: "+domain)
        comSocket.send(domain+JSON.stringify(ob));
        
        // console.log("called, waiting for return")
    }
    //Wait for the router to receive matching return call.
    //Return arrives here whether the call was made immediately or through a queue.
    return new Promise(resolve => {
        var wait = function(){
            if (router[sid]!==undefined){
                let ret = router[sid]
                router[sid] = undefined; //remove reference from routing table. 
                // console.log("got return")
                resolve(ret);
            }
            else
                setTimeout(wait,100);
        }
        setTimeout(wait, 100);
    });
    
}


function callQueue(){
    var processQueue = function(){
        if (comRoot.ready && comRoot.callQueue.length>0){
        
            console.log("calling queued function")
            comRoot.callQueue[0].src = comRoot.id; //angular and such may make calls before com has initialized
                                                //so make sure id is set in the call

            comSocket.send(comRoot.id+JSON.stringify(comRoot.callQueue[0]));
            comRoot.callQueue.splice(0,1);
            
        }
        setTimeout(processQueue,200);
    }
    setTimeout(processQueue, 200);
    
    
}
callQueue();

function sleep(ms) {
    return new Promise(resolve => setTimeout(resolve, ms));
}






///////////////

//"import" functions from external domain.
//This only wraps the callDomain call into a less verbose named call.
//The returned functions are always async.
export function extImport(domainAlias,module,fn){
    if (Array.isArray(fn)){
        //return a list of functions
        return fn.map((f)=>{return (...args)=>{return callDomain(comRoot.hostAlias[domainAlias],module,fn,args)} })
    }else{
        //return single function
        return (...args)=>{ return callDomain(comRoot.hostAlias[domainAlias],module,fn,args) }
    }
}











import socket,struct,random,json,base64
from time import sleep,time
from threading import Thread


##############################
## handle tcp messaging

def send_msg(sock, msg):
    # Prefix each message with a 4-byte length (network byte order)
    msg = struct.pack('>I', len(msg)) + msg
    sock.sendall(msg)

def recv_msg(sock):
    # Read message length and unpack it into an integer
    raw_msglen = recvall(sock, 4)
    if not raw_msglen:
        return None
    msglen = struct.unpack('>I', raw_msglen)[0]
    print("recv len",msglen)
    # Read the message data
    return recvall(sock, msglen)

def recvall(sock, n):
    # Helper function to recv n bytes or return None if EOF is hit
    data = b''
    
    while len(data) < n:
        try:
            packet = sock.recv(n - len(data))
        except ConnectionResetError:
            print("connection reset")
            packet = None
        print("packet",packet)
        if not packet:
            return None
        data += packet
    return data
###############################


root = {"pipes":{}}
callables = {}
modules = {}
mainModule = None

global alive
alive = True
def recvLoop(serversocket):
    global alive
    alive = True
    while 1: #repeatedly listen when connection is closed
        serversocket.listen(1) # become a server socket, maximum 1 connections
        print("listening")
        
        connection, address = serversocket.accept()
        root["connection"] = connection
        print("accepted")
        alive = True
        #repeatedly handle incoming messages
        while 1:
            
            recv = recv_msg(connection)
            if not recv:
                alive=False
                my_dict.pop('connection', None)
                break #connection closed
                
            print(recv)
            msg = json.loads(recv.decode())
            
            if msg["event"] == "return":
                routeReturn(msg) #pass return value (if exists) to the waiting function (if exists)
            elif msg["event"] == "call":
                # getattr(foo, 'bar')(msg)
                mainModule[msg["call"]](msg) #<<<<<<<<<<<<
                
            #call a python function synchronously. This will block the stream until value is returned.
            elif msg["event"] == "callDomain":
                # getattr(foo, 'bar')(msg)
                print(msg["args"])
                ret = getattr(mainModule,msg["call"])(*msg["args"])
                if ret == None: ret = 0
                #Ret is assumed to be a json serializable.
                #Just don't call incompatible functions
                msg["event"] = "return"
                msg["return"] = ret
                
                print("responding to",msg["src"])
                send_msg(connection,json.dumps(msg).encode())
            # else:
                # callBound(msg) #call bound messages
                



        
        
##############################
router = {}
def route(call):
    id = random.randint(100000,100000000)
    call["route_id"] = id
    message    = json.dumps(call).encode()
    data       = struct.pack('>I', len(message) ) + message
    router[id] = None
    root["connection"].sendall(data)
    return id
    
def routeReturn(msg):
    id = msg["route_id"]
    router[id] = msg["return"]

    
#blocking call that waits for the gui to return
#args and kwargs must be json serializable
def callDomain(target,fn,args):
    msg = {
        "tar":target,
        "event":"callDomain",
        "args":args,
        "call":fn,
        "src":root["connection"].domainName,
    }
    id = route(msg) #makes a call to the gui process and returns a routing id
    t0 = time()
    #wait for call to finish and return the return value
    while 1:
        if (time()-t0)>30:
            break
        if router[id]!=None:
            ret = router[id]
            router[id] = None
            return ret
        sleep(0.01)
  
 
#Non blocking call that calls js function as an event. 
#Angular and such can attach functions to the event names.
#args and kwargs must be json serializable
def callEvent(target,fn,args):
    msg = {
        "tar":target,
        "event":"callEvent",
        "args":args,
        "call":fn,
        "src":"host",
    }
    id = route(msg) #makes a call to the gui process and returns a routing id
    

   

#Python module and functions must be registered before they can be called
#
def registerCallable(fn):
    print(fn.__name__)
    callables[fn.__name__] = fn
def registerModule(mod):
    global mainModule
    print("reg",mod.__name__)
    
    modules[mod.__name__] = mod
    # root["mod"] = mod
    mainModule = mod

    
def startServer(hostName,port, block=False):
    global alive
    if "server" in root:
        root["server"].close()
    
    serversocket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    #serversocket.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
    # serversocket.bind(('localhost', 0))
    serversocket.bind((hostName, port))
    print(serversocket.getsockname()[1])
    
    root["server"] = serversocket
    
    t = Thread(target = recvLoop,    args = (serversocket,) ,  daemon = True)
    t.start()
        
    #Block until client connects. 
    #It might seem odd to do things in this manner, since serversocket already blocks by default,
    #but for convenience in other areas, we decouple it from the main process with a thread, 
    #and hence need to reblock this function, if that is what we need.
    if block:
        while 1:
            if "connection" in root:
                break
            sleep(0.01)
    
    
    #C:/programming/webkitTest
    
    return serversocket
    
def initClient(hostName,port,block=False):
    # Create a TCP/IP socket
    client = socket.socket(socket.AF_INET, socket.SOCK_STREAM)

    # Connect the socket to the port where the server is listening
    
    # sys.stdout.flush()
    
    client.connect((hostName,port))
    ob = {
            "event":"handshake",
            "domain":"py",
    }
    send_msg(client,json.dumps(ob).encode())
    
    t = Thread(target = clientRecvLoop,    args = (client,) ,  daemon = True)
    t.start()
    
    print(recv_msg(client))
    client.close()
    
def registerAsClient(modules,selfDomainName,hostName,port):
    registerModule(modules[0])
    client = TcpClientSocket(selfDomainName,hostName,port)
    root["connection"] = client
    
class TcpClientSocket():
    def __init__(self,selfDomainName,hostName,port):
        
        self.alive = True
        
        self.domainName = selfDomainName
   
        # Create a TCP/IP socket
        self.socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)

        # Connect the socket to the port where the server is listening
        
        self.socket.connect((hostName,port))
        
        #start waiting data
        t = Thread(target = self.clientRecvLoop,    args = () ,  daemon = True)
        t.start()
        
        #
        ob = {
            "event":"handshake",
            "src":"py",
        }
        send_msg(self.socket,json.dumps(ob).encode())
        
        
        
    #called in a thread. Non blocking loop
    def clientRecvLoop(self):

        self.alive = True
        #repeatedly handle incoming messages
        while 1:
            
            recv = recv_msg(self.socket)
            if not recv:
                self.alive=False
                break #connection closed
                
            print("py received",recv)
            msg = json.loads(recv.decode())
            
            if msg["event"] == "return":
                routeReturn(msg) #pass return value (if exists) to the waiting function (if exists)
            elif msg["event"] == "call":
                # getattr(foo, 'bar')(msg)
                mainModule[msg["call"]](msg) #<<<<<<<<<<<<
                
            #call a python function synchronously. This will block the stream until value is returned.
            elif msg["event"] == "callDomain":
                # getattr(foo, 'bar')(msg)
                print("call",msg["call"],msg["args"])
                ret = getattr(mainModule,msg["call"])(*msg["args"])
                if ret == None: ret = 0
                #Ret is assumed to be a json serializable.
                #Just don't call incompatible functions
                msg["event"] = "return"
                msg["return"] = ret
                
                print("responding to",msg["src"])
                send_msg(self.socket,json.dumps(msg).encode())
        print("aaaa")
        self.socket.close()
        root.pop("connection")
    
   
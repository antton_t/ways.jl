
const net  = require('net');
const http = require('http');
const url  = require('url');
const fs   = require('fs');
const path = require('path');
const WebSocket = require('ws');
const { spawn } = require('child_process');



const ROUTERDELAY = 50 //ms

function getRandomInt(min, max) {
    var min = Math.ceil(min);
    var max = Math.floor(max);
    return Math.floor(Math.random() * (max - min)) + min; //The maximum is exclusive and the minimum is inclusive
}
function getUniqueId() {
    //get 16 digit random integer within safe int range
    var min = 1000000000000000;
    var max = 9000000000000000;
    return Math.floor(Math.random() * (max - min)) + min; //The maximum is exclusive and the minimum is inclusive
}

var router = {}

var comRoot = {"ready":false
              ,"callQueue" : []
              };

//Populated with tcp, websocket, or custom handlers.
//Handler must implement handleCall and handleReturn.
//these are placeholders
var domains = {
    'nodeApp':null, //e.g. flakes app might define its own local domain for encapsulation
    'node':null,
    'py':null,
    'web':null,
}

//domain object for node. Just to unify calling conventions
function LocalDomain(domainName,module){
    this.domainName = domainName
    this.module = module
}
//called when external function calls node function
LocalDomain.prototype.handleCall = function(data){
    // console.log(this.module,data.call)
    //make a call to the requested function
    // var ret
    // if (typeof(data.call)==="string")
    var ret = this.module[data.call].apply(this,data.args)
    // else{
        // let call = data.call
        // var mod = []
        // for (var i = 0;i<data.call.length,i++){
            
        // }
    // }
    
    
    //modify the call object into a return object
    //data.event  = "return"
    //data.return = ret
    var returnDat = {
        event    : "return",
        return   : ret,
        src      : data.src,
        route_id : data.route_id,
    }
    // domains[data.src].handleReturn(dat)
    
    if (Promise.resolve(ret)===ret){
        ret.then((pret)=>{
            returnDat.return = pret
            domains[data.src].handleReturn(returnDat)
        })
    }else{
        returnDat.return = ret
        domains[data.src].handleReturn(returnDat)
    }
    
}
//Called when call to an external function returns (the call was made by node)
//Original call set up a promise that monitors router table, and resolves once
//the right route_id is found.
LocalDomain.prototype.handleReturn = function(data){
    router[data.route_id] = data.return
}



//Used when connecting to a host process tcp server.
//I.e. node is not the host
function TcpHandler(host,port){
    var self = this;
    self.messageRemaining = 0;
    self.messagePtr       = 0;
    self.host = host;
    self.port = port;
    
    //create a client socket. Python (or any other host) is generally the server
    var socket  = new net.Socket();
    socket.connect(port, host, ()=>{self.connected()});

    socket.on('data', function(data) {
        //console.log('RAWDATA: ' + data.slice(4));
        self.recv(data)
    });
    
    
    socket.on('error', function(err) {
        if(err.code === 'ECONNREFUSED'){
            console.log("Could not connect, retrying (server might not be active yet)")
            //fat arrow => is lambda basically
            setTimeout(()=>{socket.connect(port, host)}, 2000)
        }
    });
    
    socket.on('close', function() {
        console.log('Connection closed');
    });
    
    self.socket = socket;
}
TcpHandler.prototype.connected = function(){
    console.log(this.host)
    console.log('CONNECTED TO: ' + this.host + ':' + this.port);
}
TcpHandler.prototype.newMessage = function(n,data){
    this.message          = Buffer.alloc(n);
    this.messageSize      = n;  //size of the message, excluding the header
    this.messagePtr       = 0;  //where the next bytes should be inserted
    this.messageRemaining = n;  //how many bytes are left to read    
}
TcpHandler.prototype.appendMessage = function(n,data){
    data.copy(this.message, this.messagePtr,0,n);
    this.messagePtr       += n;
    this.messageRemaining -= n;
    if(this.messageRemaining<0){
        console.log("shouldn't happen");
    }
}
TcpHandler.prototype.finalize = function(){
    console.log('COMPLETE: ' +  this.message.toString());
    var t = JSON.parse(this.message);
    console.log(t.event);
    handleMessage(t)
    console.log("=================");
}
TcpHandler.prototype.recv = function(data){
    
    //if data is new message
    if (this.messageRemaining === 0){
        //next 4 bytes are known to be the message size
        var n = data.readUIntBE(0, 4); //read big endian integer. 4 bytes
        this.newMessage(n);
        if (data.length===4) return; //got only header. do nothing else
        
        this.recv(data.slice(4))
        
    //if the data completes a message (or is entirely complete message alone)
    }else if (this.messageRemaining === data.length){
        this.appendMessage(data.length, data);
        this.finalize();
    //if data is data to append to existing partial message, but does not complete the message 
    }else if (this.messageRemaining > data.length){
        this.appendMessage(data.length,data);
    //if data completes a message, but contains parts of next message
    }else if (this.messageRemaining < data.length){
        var msg     = data.slice(0, this.messageRemaining);
        var nextMsg = data.slice(this.messageRemaining);
        this.appendMessage(this.messageRemaining,data);
        this.finalize();
        this.recv(nextMsg);
    }
}
TcpHandler.prototype.handleCall = function(data){
    var dat = JSON.stringify(data)
    
    //store message size as big endian integer (standard in networking, nonstandard everywhere else)
    var size = Buffer.alloc(4)
    size.writeUInt32BE(dat.length)
    
    this.socket.write(size) //send header
    this.socket.write(dat) //send data
}
TcpHandler.prototype.handleReturn = function(data){
    var dat  = JSON.stringify(data)
    
    //store message size as big endian integer (standard in networking, nonstandard everywhere else)
    var size = Buffer.alloc(4)
    size.writeUInt32BE(dat.length)
    
    this.socket.write(size) //send header
    this.socket.write(dat) //send data
}







function TcpServer(legalDomains,hostName,port){
    var self = this;
    
    self.hostName = hostName;
    self.legalDomains = legalDomains;
    self.clients = {}
    
    //create a server socket.
    var socket = net.createServer(function(client) {
        self.latestClient = new TcpCLientSocket(self,client)
        
        
    });
    self.socket = socket;
    
    socket.on('close', function() {
        console.log('Server socket closed');
    });
    socket.on('error', function (e) {
        console.log("socket server error:")
        console.log(e)
    })
    //it takes few ms to check port availability, hence store a promise to variable
    //so calling code doesn't get ahead of itself.
    self.ensurePort = new Promise(r=>{
        if(port===0 || port===undefined){
            getRandomPort(randPort=>{
                self.port = randPort;
                socket.listen(randPort, 'localhost');
                r(randPort)
            })
        }else{
            self.port = port;
            socket.listen(port, 'localhost');
            self.socket = socket;
            r(port)
        }
    })
    
}
TcpServer.prototype.connected = function(){
    console.log(this.host)
    console.log('CONNECTED TO: ' + this.host + ':' + this.port);
}







//this class is always instantiated by the TcpServer class when client connects
function TcpCLientSocket(server,socket){
    var self = this;
    self.socket = socket
    self.server = server
    self.messageRemaining = 0;
    self.messagePtr       = 0;
    self.domainName = null //assigned on handshake
    
    //this client sends data
    socket.on('data', function(data) {
        // console.log('RAWDATA: ' + data.slice(4));
        self.recv(data)
    });
    
    //this client disconnects (server stays alive)
    socket.on('end', function () {
        //self.clients.splice(self.clients.indexOf(client), 1);
        console.log("tcp client disconnected")
        domains[self.domainName] = null
        self.server.clients[self.domainName] = undefined
    });
    
}
TcpCLientSocket.prototype.newMessage = function(n,data){
    this.message          = Buffer.alloc(n);
    this.messageSize      = n;  //size of the message, excluding the header
    this.messagePtr       = 0;  //where the next bytes should be inserted
    this.messageRemaining = n;  //how many bytes are left to read    
}
TcpCLientSocket.prototype.appendMessage = function(n,data){
    data.copy(this.message, this.messagePtr,0,n);
    this.messagePtr       += n;
    this.messageRemaining -= n;
    if(this.messageRemaining<0){
        console.log("shouldn't happen");
    }
}
TcpCLientSocket.prototype.finalize = function(){
    // console.log('COMPLETE: ' +  this.message.toString());
    var t = JSON.parse(this.message);
    console.log(t.event);
    handleMessage(t,this)
    console.log("=================");
}
TcpCLientSocket.prototype.recv = function(data){
    //if data is new message
    if (this.messageRemaining === 0){
        //next 4 bytes are known to be the message size
        var n = data.readUIntBE(0, 4); //read big endian integer. 4 bytes
        this.newMessage(n);
        if (data.length===4) return; //got only header. do nothing else
        
        this.recv(data.slice(4))
        
    //if the data completes a message (or is entirely complete message alone)
    }else if (this.messageRemaining === data.length){
        this.appendMessage(data.length, data);
        this.finalize();
    //if data is data to append to existing partial message, but does not complete the message 
    }else if (this.messageRemaining > data.length){
        this.appendMessage(data.length,data);
    //if data completes a message, but contains parts of next message
    }else if (this.messageRemaining < data.length){
        var msg     = data.slice(0, this.messageRemaining);
        var nextMsg = data.slice(this.messageRemaining);
        this.appendMessage(this.messageRemaining,data);
        this.finalize();
        this.recv(nextMsg);
    }
}
TcpCLientSocket.prototype.handleCall = function(data){
    var dat = JSON.stringify(data)
    
    //store message size as big endian integer (standard in networking, nonstandard everywhere else)
    var size = Buffer.alloc(4)
    size.writeUInt32BE(dat.length)
    
    this.socket.write(size) //send header
    this.socket.write(dat) //send data
}
TcpCLientSocket.prototype.handleReturn = function(data){
    var dat  = JSON.stringify(data)

    //store message size as big endian integer (standard in networking, nonstandard everywhere else)
    var size = Buffer.alloc(4)
    size.writeUInt32BE(dat.length)
    
    this.socket.write(size) //send header
    this.socket.write(dat) //send data
}
TcpCLientSocket.prototype.finalizeHandshake = function(dat,ob){
    var id = this.server.legalDomains.indexOf(dat.src)
    console.log("clientSocket handshake",JSON.stringify(dat))
    if (id !== -1){
        domains[dat.src] = this
        if("subDomains" in dat){
            //register all sub domains
            dat.subDomains.forEach(sub => {
                let id = this.server.legalDomains.indexOf(sub)
                if (id !== -1){
                    domains[sub] = this
                }else{
                    console.log("sub domain name not registered:",sub)
                }
            })
        }
        this.domainName = dat.src
        this.server.clients[this.domainName] = this
        this.handleCall(ob)
    }
}




function WebIoHandler(legalDomains,port){
    //console.log(httpServer)
    var self = this;
    self.clients = {}
    // self.domainName = domainName
    // if (typeof()==="string")
    self.legalDomains = [].concat(legalDomains)
    console.log("starting websocket server")
    
    var wss = new WebSocket.Server({ port: port });
    
    wss.on('connection', function(ws) {
        //wrap the client socket to domain handler class
        self.latestClient = new WebSocketClient(self,ws) 
    });
    self.server = wss
    
}

//this wrapper class is always instantiated by the WebIoHandler class when client connects
function WebSocketClient(server,socket){
    var self = this;
    self.server = server;
    self.socket = socket;
    socket.on('message', function(message) {
        // console.log('socket received: %s', message);
        handleMessage(JSON.parse(message),self)
    });
    
    
    //this client disconnects (server stays alive)
    socket.on('close', function () {
        //self.clients.splice(self.clients.indexOf(client), 1);
        console.log("websocket client disconnected")
        domains[self.domainName] = null
        self.server.clients[self.domainName] = undefined
    });
    socket.on('error', function (e) {
        if (e.code !== 'ECONNRESET') {
            // ignore connection reset error, but re-throw any other error.
            throw e
        }
    })
}
WebSocketClient.prototype.handleCall = function(data){
    this.socket.send(JSON.stringify(data));
}
WebSocketClient.prototype.handleReturn = function(data){
    // console.log("sending", JSON.stringify(data))
    this.socket.send(JSON.stringify(data));
}
WebSocketClient.prototype.finalizeHandshake = function(dat,ob){
    var id = this.server.legalDomains.indexOf(dat.src)
    if (id !== -1){
        domains[dat.src] = this
        this.domainName = dat.src
        this.socket.send(JSON.stringify(ob))
    }else{
        console.log("invalid domain",dat.src)
    }
}




//get port in a range starting from 45032
var portrange = 45032
function getPort (cb) {
    var port = portrange
    portrange += 1
   
    var server = http.createServer()
    server.listen(port, function (err) {
        server.once('close', function () {
            cb(port)
        })
        server.close()
    })
    server.on('error', function (err) {
        getPort(cb)
    })
}
function getRandomPort (cb) {
    var server = http.createServer()
    server.listen(0, function (err) {
        var port = server.address().port
        server.once('close', function () {
            cb(port)
        })
        server.close()
    })
    server.on('error', function (err) {
        getRandomPort(cb)
    })
}



// var stuff = require('./stuff.js');
// stuff.hello()
//var handler = new TcpHandler('localhost',60942);

// getRandomPort( (port)=>{
// var port = 9000;

//Minimalist http server. One can also use Express instead of this.
//Or just run a http server from startup script before launching the index module
function startHttpServer(){
    return http.createServer(function (req, res) {
        console.log(`${req.method} ${req.url}`);
        // parse URL
        const parsedUrl = url.parse(req.url);
        // extract URL path
        let pathname = `.${parsedUrl.pathname}`;
        //disallow accessing locations below current folder
        pathname = pathname.replace(/^(\.)+/, '.'); 
        
        // maps file extention to MIME types
        const mimeType = {
            '.ico': 'image/x-icon',
            '.html': 'text/html',
            '.js': 'text/javascript',
            '.json': 'application/json',
            '.css': 'text/css',
            '.png': 'image/png',
            '.jpg': 'image/jpeg',
            '.wav': 'audio/wav',
            '.mp3': 'audio/mpeg',
            '.svg': 'image/svg+xml',
            '.pdf': 'application/pdf',
            '.doc': 'application/msword',
            '.eot': 'appliaction/vnd.ms-fontobject',
            '.ttf': 'aplication/font-sfnt'
        };
        
        
        
        
        fs.exists(pathname, function (exist) {
            if(!exist) {
                // if the file is not found, return 404
                res.statusCode = 404;
                res.end(`File ${pathname} not found!`);
                return;
            }
            // if is a directory, then look for index.html
            if (fs.statSync(pathname).isDirectory()) {
                pathname += '/index.html';
            }
            // read file from file system
            fs.readFile(pathname, function(err, data){
                if(err){
                    res.statusCode = 500;
                    res.end(`Error getting the file: ${err}.`);
                } else {
                    // based on the URL path, extract the file extention. e.g. .js, .doc, ...
                    const ext = path.parse(pathname).ext;
                    // if the file is found, set Content-type and send data
                    res.setHeader('Content-type', mimeType[ext] || 'text/plain' );
                    res.end(data);
                }
            });
        });

    })
}
//router waits for the (possibly remote) call to return, 
//and routes the return value to the calling domain/function
function route(id){
    var sid = ""+id;
    router[sid] = undefined
    
    //Wait for the router to receive matching return call.
    //Return arrives here whether the call was made immediately or through a queue.
    return new Promise(resolve => {
        var wait = function(){
            if (router[sid]!==undefined){
                resolve(router[sid]);
            }
            else
                setTimeout(wait,ROUTERDELAY);
        }
        setTimeout(wait, ROUTERDELAY);
    });
}

//domain servers/clients receive and preprocess the message, then call this.
function handleMessage(dat,caller){
    //When host or middle manager calls js function.
    //The function must exists in the visible scope
    if(dat.event === "callDomain"){
        //domain objects all implement handleCall
        // console.log(dat.tar)
        
        try{
            // console.log(dat.tar)
            domains[dat.tar].handleCall(dat)
        }catch(e){
            if (!(dat.tar in domains)) console.log("domain not found:",dat.tar)
            console.log(e)
        }
    }
    //when host or middle manager calls js function as an event
    else if(dat.event === "callEvent"){
        var ev = new CustomEvent(dat.call,  { detail: dat.args });
        window.dispatchEvent(ev);
    }
    else if(dat.event === "return"){
        //console.log("got return from "+dat.tar);
        console.log("got return");
        //pass the message to the router. Pending promises will pick up
        domains[dat.src].handleReturn(dat)
    }else if (dat.event === "handshake"){
        comRoot.ready = true; //<<< a bit dubious
        let ob = {event:"handshake",fromDomain:"node"}
        
        //console.log(Object.keys(domains))
        // domains[dat.domain].socket.send(JSON.stringify(ob))
        // domains[dat.domain].finalizeHandshake(dat,ob)
        caller.finalizeHandshake(dat,ob)
    }else if(dat.event === "msg"){
        //$("#bonk").text("message: "+dat.val )
        console.log("message: "+dat.val)
    }
}

//>>>>>add ability to add multiple modules
function registerModule(domainName, module){
    domains[domainName] = new LocalDomain(domainName,module)
}

function registerDomains(domainNames,domainType,port){
    if (domainType==="webSocketServer"){
        return new WebIoHandler(domainNames,port)
        // domains[domainName] = new WebIoHandler(domainNames,port)
        
    //some other process maintains tcp server
    }else if (domainType==="tcpClient"){
        return new TcpHandler("localhost",port)
        // domains[domainName] = new TcpHandler("localhost",port)
    
    //node maintains tcp server, some other process is client
    }else if (domainType==="tcpServer"){
        return new TcpServer(domainNames,'localhost',port)
        // domains[domainName] = new TcpServer("localhost",port)
    }
}

function startServer(port){
    var app = startHttpServer()
    app.listen(port)
    
    return app;
}
function startHeadless(){
    //todo
}
// });

//do a quasi-synchronous call to external function
function callDomain(domain,fun,args){
    var id = getUniqueId();
    var sid = ""+id;
    
    var ob = {
       "tar":domain,
       "src":"node",
       "event":"callDomain",
       "args":args,
       "call":fun,
       "route_id":id,
    }
    
    router[sid] = undefined
    
    //call external domain
    domains[domain].handleCall(ob)
    
    //wait for the function to return, then resolve the promise
    return new Promise(resolve => {
        var wait = function(){
            if (router[sid]!==undefined){
                resolve(router[sid]);
            }
            else
                setTimeout(wait,ROUTERDELAY);
        }
        setTimeout(wait, ROUTERDELAY);
    });
    
}


function startChildProc(){
    child.stdout.on('data', (data) => {
        console.log(`child stdout:\n${data}`);
    });

    child.stderr.on('data', (data) => {
        console.error(`child stderr:\n${data}`);
    });
}






exports.TcpHandler = TcpHandler
exports.callDomain = callDomain
exports.registerDomains = registerDomains
exports.registerModule = registerModule
exports.startServer = startServer
exports.domains = domains
exports.getRandomPort = getRandomPort

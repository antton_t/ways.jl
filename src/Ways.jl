

module Ways

import HTTP
import HTTP:startwrite
import Sockets
import WebSockets
import WebSockets:WebSocketLogger,subprotocol,readguarded,writeguarded
import JSON
import JSON:json

const J = JSON

using Logging

global_log = global_logger()

#It's not necessary to use this. This is mostly for testing.
function setLogger(logger)
    global global_log
    global_log = logger
end

##############################
######### HTML host  #########
##############################


# map file extentions to MIME types
mimeTypes = Dict(
    ".ico" => "image/x-icon",
    ".html"=> "text/html",
    ".js"  => "text/javascript",
    ".json"=> "application/json",
    ".css" => "text/css",
    ".png" => "image/png",
    ".jpg" => "image/jpeg",
    ".wav" => "audio/wav",
    ".mp3" => "audio/mpeg",
    ".svg" => "image/svg+xml",
    ".pdf" => "application/pdf",
    ".doc" => "application/msword",
    ".eot" => "appliaction/vnd.ms-fontobject",
    ".ttf" => "aplication/font-sfnt",
    ".map" => "application/octet-stream"
)


domains      = Dict{String,Any}()
servers      = Dict{Symbol,Any}()

thisDomain = "jl"

strDocs = Dict{String,String}()


const ROUTERDELAY = 0.3 #seconds   how frequently router checks incoming values
router = Dict{String,Any}()


struct retValue
    value::Any
end


function getExtDomains()
    domains
end



#crude parsing function. The splitpath function in HTTP module doesn't seem to work for this purpose.
function parseUrl(url)
    url = startswith(url,"/") ? url[2:end] : url 
    
    paramSplit = split(url,"?")
    if length(paramSplit)<2 
        params = nothing
    else
        paramsS = paramSplit[2]
        if length(paramsS)>0
            mid     = split(paramsS,"&")
            params  = Dict{String,String}()
            paramsL = []
            for pair in mid
                items = split(pair,"=")
                if length(items)<2; continue; end #invalid parameter
                k,v = items
                if length(k)==0 || length(v)==0; continue; end #invalid parameter

                params[k] = v
            end
        else
            params = nothing
        end
    end
    
    
    path  = paramSplit[1]
    parts = split(path,"/")
    
    Dict(
        "fullPath"=>path
        ,"docName"=>parts[end]
        ,"params"=>params
        ,"parts"=>parts
    )

end

function httpRequestHandler(req::HTTP.Request)
    
    parsedUrl = parseUrl(req.target)
    
    try 
        s = open("./"*parsedUrl["fullPath"]) do file
            read(file, String)
        end
        
        #@show s
        path,ext = splitext(parsedUrl["docName"])
        #@info ext
        mime = mimeTypes[ext]
        
        res = HTTP.Response(200, s)
        HTTP.setheader(res, "Content-Type" => mime)
        return res
    catch e
        @show e
        return HTTP.Response(404)
    end
    
end


function ver()
    println("ver",1)
    
end

function docs()
    return strDocs
end


#create or update string document in memory
function strDocInitHandler(req::HTTP.Request)
    doc = J.parse(IOBuffer(HTTP.payload(req)))
    
    mime = "text/html"
    res = HTTP.Response(200, "ok")
    HTTP.setheader(res, "Content-Type" => mime)
    
    strDocs[doc["name"]] = doc["content"]
    
    return res
end

function strDocRequestHandler(req::HTTP.Request)
    try
        parsedPath = parseUrl(req.target)
        
        strDoc = strDocs[parsedPath["docName"]]
        
        local mime
        if endswith(parsedPath["docName"],".js")
            mime = "text/javascript"
        elseif endswith(parsedPath["docName"],".css")
            mime = "text/css"
        else
            mime = "text/html"
        end
        res = HTTP.Response(200, strDoc)
        HTTP.setheader(res, "Content-Type" => mime)
        return res
        
    catch e
        @info "error in str handler:" e
        return HTTP.Response(404)
    end
    
    
end


#returns a handler function that serves files from path0
function routingPath(path0)
    function pathRequestHandler(req::HTTP.Request)
    
        parsedUrl = parseUrl(req.target)
        try 
            
            filePath = joinpath( path0, parsedUrl["parts"][2:end]... )
            s = open(filePath) do file
                read(file, String)
            end
            
            path,ext = splitext(parsedUrl["docName"])
            
            mime = mimeTypes[ext]
            
            res = HTTP.Response(200, s)
            HTTP.setheader(res, "Content-Type" => mime)
            return res
        catch e
            #@show e
            bt  = catch_backtrace()
            msg = sprint(showerror, e, bt)
            println(msg)
            
            
            return HTTP.Response(404)
        end
    
    end 
    return pathRequestHandler
end




###################################
######### Domain handling #########
###################################




mutable struct LocalDomain
    domainName::String
    modules::Dict{String,Module}
    LocalDomain() = new()
    LocalDomain(domainName::String,modules::Dict{String,Module}) = new(domainName,modules)
end

function registerModule_(domain::LocalDomain, modName::String, module_::Module)
    
end

function handleCall(domain::LocalDomain, data)
    #make a call to the requested function
    
    modRef = domain.modules[data["module"]]
    fun = getfield( modRef, Symbol(data["call"]) )
    
    #init a return object
    returnDat = Dict{String,Any}(
        "event"    => "return",
        "src"      => data["src"],
        "route_id" => data["route_id"],
        "return"   => nothing,
    )
    
    local ret
    
    try
        ret = fun( data["args"]... )
        
        if isa(ret,Channel) #is a channel? Call is handled asynchronously
            ret2 = take!(ret) #blocks until ret has a value. ret is bound to its task and is closed automatically.
            returnDat["return"] = ret2
        else
            returnDat["return"] = ret
        end
    catch e
        bt  = catch_backtrace()
        msg = sprint(showerror, e, bt)
        println(msg)
    end
    handleReturn(domains[data["src"]], returnDat)
    
end
#NOTE: handleCall of LocalDomain above does not usually call this! handleCall calls a matching method where domain is of different type (e.g. WsServerConnection).
#This is called when call to an external function returns (original call was made by julia)
#Original call set up a Task that monitors router table, and resolves once
#the right route_id is found.
function handleReturn(domain::LocalDomain, data)
    #return the value via routing channel and remove the channel from router
    put!(router[data["route_id"]], data["return"])
    pop!(router,data["route_id"])
    # close(router[data["route_id"]])
end


function disconnect!(domain::LocalDomain)
    #empty dummy function to simplify mass restart.
end









mutable struct WebSocketServer
    server::Any
    legalDomains::Array{String}
    clients::Dict{String,Any}
    serverTask::Task
    WebSocketServer() = new()
end

function WebSocketServer(legalDomains::Array{String},port)
    serverWs = WebSocketServer()
    serverWs.clients = Dict()
    
    #The server will reject any identifier, that is not in legalDomains list.
    serverWs.legalDomains = copy(legalDomains) 
    
    @info "starting websocket server"
    
    #Dummy handler for http requests to localhost:port
    #Http requests are handled separately. Nothing should ever call this.
    httpDummy(req) = WebSockets.Response("httpdummy")
    
    
    #called by the server
    function WsServerHandler(req, ws)
        #Client has jus connected. ws is different for each connection.

        println("============================")
        
        orig = WebSockets.origin(req)
        
        connection = WsServerConnection(ws,serverWs,"")

        @info "ways: Started websocket connection" ws isopen(ws)
        
        while isopen(ws)
            data, = readguarded(ws) #blocks until a message has been received
            s = String(data)
            # @info "Received" s
            if s == ""
                break
            end
            try 
                #call the generic message handler
                @async handleMessage(s,connection)
            catch e
                bt  = catch_backtrace()
                msg = sprint(showerror, e, bt)
                println(msg)
            end
        end

        @info "ways: Websocket will now close " ws
        
        flush(stdout)
        flush(global_log.stream)
    end
    
    wss = WebSockets.ServerWS(httpDummy,  WsServerHandler)
    serverWs.server = wss
    
    #start servering
    serverWs.serverTask = @async WebSockets.serve(wss, Sockets.localhost, port)
    serverWs
end

function disconnect!(server::WebSocketServer)
    for (domName,connection) in server.clients
        disconnect!(connection)
    end
    close(server.server)
end



#Wrapper to the socket that we push data to, when interacting with the client.
mutable struct WsServerConnection
    socket::WebSockets.WebSocket
    server::WebSocketServer
    domainName::String         #name of the client domain connecting to the ws server
end

#julia code is calling a function in external domain. 
#Note, that returning value goes to the routing table via handleReturn(::LocalDomain...), and is read from there by the "callDomain" function, which called this function.
#  callDomain starts an async task for this.
#The handleReturn function below is not involved in this process. Perhaps these are confusing names.
function handleCall(connection::WsServerConnection, data)
    writeguarded(connection.socket, JSON.json(data))
end

#external websocket domain has previously called julia function (or there has been a domain hop). Now we are returning a value to that domain.
function handleReturn(connection::WsServerConnection, data)
    #@info "returning" data
    writeguarded(connection.socket, JSON.json(data))
end

#finalize handshake and respond to the client
function finalizeHandshake(connection::WsServerConnection, messageData)
    if messageData["src"] in connection.server.legalDomains #is the connecting domain recognizable?
        handshakeObject = Dict("event"=>"handshake", "fromDomain"=>thisDomain, "src"=>thisDomain)
    
        domains[messageData["src"]] = connection            #register this connection as domain
        connection.domainName       = messageData["src"]
        
        writeguarded(connection.socket, JSON.json(handshakeObject))
    else
        @info """invalid domain $(messageData["src"])."""
    end
end

function disconnect!(WsServerConnection)
    close(WsServerConnection.socket)
end



#client for interacting with server in another domain
mutable struct WebSocketClient
    socket::Any
    port::Int
    uri::String
    domainName::String
    handshakeResponse::Channel
    WebSocketClient() = new()
end

    
#Wrapper to the socket that we push data to, when interacting with the server.
function WebSocketClient(domainName::String,port::Int)
    client = WebSocketClient()

    client.uri  = "ws://localhost:$(port)"
    client.port = port
    client.domainName = domainName
    
    client.handshakeResponse = Channel(1)
    
    @async WebSockets.open(client.uri) do ws
        client.socket = ws
        status = requestHandshake(client) #blocks until handshake received (or there's error)
        recvLoop(client) 
        # @info "ways: WebSocketClient connected:" status client.domainName isopen(ws)
    end
    
    take!(client.handshakeResponse) #block until handshake is done
    
    
    client
end
function recvLoop(self::WebSocketClient)
    #iteration blocks until next one is received. loops indefinitely.
    while true
        msg, success = readguarded(self.socket) #blocks until message reveived
        s = String(msg)
        if success && s!= ""
            @async handleMessage(s,self)
        else
            @info "websocket client closed"
            break
        end
    end
    @info "end"
end
#julia code is calling a function in external domain. 
#Note, that returning value goes to the routing table via handleReturn(::LocalDomain...), and is read from there by the "callDomain" function, which called this function.
#  callDomain starts an async task for this.
#The handleReturn function below is not involved in this process. Perhaps these are confusing names.
function handleCall(self::WebSocketClient, data)
    writeguarded(self.socket, JSON.json(data))
end

#external websocket domain has previously called julia function (or there has been a domain hop). 
#Now we are returning a value to that domain.
function handleReturn(self::WebSocketClient, data)
    writeguarded(self.socket, JSON.json(data))
end

function requestHandshake(self::WebSocketClient)
    #let d = new Date();
    global thisdomain
    data = Dict(
        "event"=> "handshake",
        "src"  => thisDomain,
        #time : d.getHours()+":"+d.getMinutes()+":"+d.getSeconds()
    )
    
    did = writeguarded(self.socket, JSON.json(data))
    
    msg,success = readguarded(self.socket)
    s = String(msg)
    
    handleMessage(s,self)
    
    return true
end

#finalize handshake (called after server has confirmed handshake request)
function finalizeHandshake(self::WebSocketClient, messageData)
    try
        println("ways: handshake from: $(messageData["src"]) (port:$(self.port)). Registering as $(self.domainName)")
        domains[self.domainName] = self   #register this connection as domain
        put!(self.handshakeResponse, true)
    catch e
        put!(self.handshakeResponse, false)
        bt  = catch_backtrace()
        msg = sprint(showerror, e, bt)
        println(msg)
    end
end

#<<<<<
function disconnect!(self::WebSocketClient)
    close(self.socket)
end
        




function getUniqueId()
    rand(1:2^63 - 1)
end

function initLocalDomain(domain::String)
    global thisDomain
    thisDomain = domain
    #loc = LocalDomain()
    #loc.domainName = domain
    loc = LocalDomain(domain,Dict{String,Module}())
    domains[domain] = loc
end

#Register julia module in the given domain.
#Multiple local domains can be registered with different names and different associated modules
function registerModule(domain::String,moduleRegName,module_)
    #Register domain module path for given module
    #Module registration name can be different from the actual module name.
    #e.g. jl.main:mymodule
    #domainName = "$domain:$moduleRegName"

    #Add a domain and a module, or just a module.
    if !haskey(domains,domain)
        domains[domain] = LocalDomain(domain,Dict(moduleRegName=>module_))
    else
        domains[domain].modules[moduleRegName] = module_
    end
    
    
end


function registerDomains(domainNames,domainType,port)
    global servers
    if (domainType=="webSocketServer")
        srv = WebSocketServer(domainNames,port)
        servers[:self] = srv
        return srv
    elseif (domainType=="webSocketClient")
        return WebSocketClient(domainNames,port)
        # domains[domainName] = new WebIoHandler(domainNames,port)
    #some other process maintains tcp server
    elseif (domainType=="tcpClient")
        return TcpHandler("localhost",port)
        # domains[domainName] = new TcpHandler("localhost",port)
    
    #node maintains tcp server, some other process is client
    elseif (domainType=="tcpServer")
        return TcpServer(domainNames,"localhost",port)
        # domains[domainName] = new TcpServer("localhost",port)
    end
end

#Closes all websocket connections and clears all domains
#Does not close the server.
function unregisterAll()
    for (name,dom) in domains
        disconnect!(dom)
        
        delete!(domains,name)
    end
    
end







#Message json data is prefixed with target domain name.
#This allows quick target name parsing for domain hopping without having to parse the information from json.
#<<<NOTE: this is not fully implemented. 
#         still missing: forwarding/re-returning calls, 
#                        returning json is not prefixe with target domain.
function splitMessage(message)
    e = 1
    l = length(message)
    while e<=l && message[e]!='{'
        e+=1
    end
    domain = SubString(message,1,e-1)
    dataS  = SubString(message,e,l)

    (domain,dataS)
end

#Messages first arrive at registered server-capable domains (localdomain, websocketserver, tcpserver)
#Message is received in full by those specific handlers, and then passed here.
#  (How message receiving is handled depends on the protocol)
# usual pathway might be e.g.
#  -websocket client (e.g. browser) sends message to julia websocket server, where a WebsocketClientConnection receives it.
#  -handleMessage (the function below) asynchronously calls LocalDomain (wrapping julia module), which calls the given function.
#  -LocalDomain waits for the function to return and then calls WebsocketClientConnection return-handler directly. (handleMessage receives nothing) 
#  -WebsocketClientConnection sends message to the original websocket.
#  -On browser side, the message is interpreted as a return value to the original function
#     -calls do not block each other. Any number of async calls can be made independently, and return in any order.
#     -a routing table is used to match return message to their calling functions. 
function handleMessage(message,caller)
    #message is likely a handshake. The client doesn't know host domain name yet, so domain target is omitted.
    if message[1] == '{'
        targetDomain = thisDomain
        data = JSON.parse(message)
        dataS = ""
    else
        targetDomain,dataS = splitMessage(message)
        #@info "dump" dataS
        data = JSON.parse(dataS)
    end
    #@info "tardomain" targetDomain thisDomain data
    if targetDomain==thisDomain
        if data["event"]=="callDomain"
            #<<<add check for domain validity
            #multiple dispatch
            try
                handleCall(domains[data["tar"]],data)
            catch e
                bt  = catch_backtrace()
                msg = sprint(showerror, e, bt)
                println(msg)
            end
        
        elseif data["event"]=="return"
            #Note: src is the domain that made the call. In most (all?) cases this is thisDomain, and domains[data["src"]] is a LocalDomain.
            #<<<this probably could be hardcoded since no other domain handles return here.
            #<<<  lets note that if there are multiple jl domains, they need to use different identifiers.
            try
                handleReturn(domains[data["src"]], data)
            catch e
            
                
                bt  = catch_backtrace()
                msg = sprint(showerror, e, bt)
                println(msg)
                
            end
        elseif data["event"]=="callEvent"
            #<<< does this serve any purpose?
        elseif data["event"]=="handshake"
            #note: multiple dispatch based on caller type!
            finalizeHandshake(caller,data)
        end
        
    else
        #currently domain hopping is unhandled
        @info "unknown domain " targetDomain
    end
    flush(stdout)
    flush(global_log.stream)
end





#do a quasi-synchronous call to external function
function callDomain(domain,module_,fun,args)
    id  = getUniqueId();
    sid = string(id);
    
    ob = Dict(
       "tar"     =>domain,
       "src"     =>thisDomain,
       "event"   =>"callDomain",
       "args"    =>args,
       "module"  =>module_,
       "call"    =>fun,
       "route_id"=>sid,
    )
    
    ch = Channel(1)
    router[sid] = ch
    
    #handleReturn is responsible for removing the channel from the router.
    #Currently only outbound calls from localdomain need to worry about this.
    #If domainhopping is implemented, that needs to account for this as well.
    
    #call external domain
    handleCall(domains[domain], ob)
    
    return ch
end

#check if revise is working
function test()
    println("test8")
end


end
